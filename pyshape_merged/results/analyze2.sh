#! /bin/bash


x=`ls -1 *.txt | sort -V -t '_' -k 1`

echo -e "${x}" | cut -d '_' -f 3 > ./dip1
echo -e "${x}" | cut -d '_' -f 4 | sed 's/....$//' > ./dip2
grep Fidelity ${x} | cut -d '=' -f 2 > fid

paste dip1 dip2 fid > output_dips

rm dip1 dip2 fid

