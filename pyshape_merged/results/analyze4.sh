#! /bin/bash


x=`ls -1 *.txt | sort -V -t '_' -k 1`

echo -e "${x}" | cut -d '_' -f 3 | sed 's/....$//' > ./wc
grep 'Fidelity' ${x} | cut -d '=' -f 2 > ./fid

paste wc fid > output_AreaVOCC

rm wc fid

