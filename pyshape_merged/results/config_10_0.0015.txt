
Calculating LA Phonon EID Kernel...
...calculation complete.
time dependence for twodottwoleveldm gate using ampmask_none and phasemask_cos

Fidelity = 0.9700622318230783
phi_alpha = 0.312 PI radians
phi_gamma = 235.0 fs
phi_delta = 0.373 PI radians
pulse_area = 3.25 PI radians
