#! /bin/bash


x=`ls -1 *.txt | sort -V -t '_' -k 1`

echo -e "${x}" | cut -d '_' -f 3 > ./dip
echo -e "${x}" | cut -d '_' -f 4 > ./det
echo -e "${x}" | cut -d '_' -f 5 | cut -d '.' -f 1 > w
grep Fidelity ${x} | cut -d '=' -f 2 > fid

paste dip det w fid > output

rm dip det w fid

