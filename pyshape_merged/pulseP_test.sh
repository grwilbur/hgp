#!/bin/bash

if [ -d "results" ]
then

    
    rm -r ./results/*.txt
    
else

    mkdir results

fi


if [ -d "configs" ]
then

    rm -r ./configs/*

else
    
    mkdir configs

fi

wc=($(seq 0.1 0.1 4)) #eV
i=0
#Create a directory with many config files

for w in "${wc[@]}"
do
	change=$(sed  "129s/\(^.*pulse_area.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $w  \2/" config.ini)
	echo -e "$change" > "./configs/config_${i}_${w}.ini"
	echo -e "$change" | sed '129q;d'
	i=`echo "$i + 1" | bc`
done

#use paralel to run processes at the same time

parallel -j+0 --eta python2 main.py {} ">" ./results/{/.}.txt ::: ./configs/*.ini



