#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:54:03 2016

@author: ajan
"""

import sympy
import numpy

#density matrix elements
P_00=sympy.symbols('P_00')
P_0x=sympy.symbols('P_0x')
P_0y=sympy.symbols('P_0y')
P_0b=sympy.symbols('P_0b')
P_x0=sympy.symbols('P_x0')
P_xx=sympy.symbols('P_xx')
P_xy=sympy.symbols('P_xy')
P_xb=sympy.symbols('P_xb')
P_y0=sympy.symbols('P_yo')
P_yx=sympy.symbols('P_yx')
P_yy=sympy.symbols('P_yy')
P_yb=sympy.symbols('P_yb')
P_b0=sympy.symbols('P_b0')
P_bx=sympy.symbols('P_bx')
P_by=sympy.symbols('P_by')
P_bb=sympy.symbols('P_bb')

mu=sympy.symbols(r'$\lambda$')

#Hamiltonian elements
H_00=0#sympy.symbols('H_00')
H_0x=sympy.symbols('H_0x')
H_0y=sympy.symbols('H_0y')
H_0b=0#sympy.symbols('H_0b')
H_x0=sympy.symbols('H_x0')
H_xx=sympy.symbols('H_xx')
H_xy=0#sympy.symbols('H_xy')
H_xb=sympy.symbols('H_xb')
H_y0=sympy.symbols('H_yo')
H_yx=0#sympy.symbols('H_yx')
H_yy=sympy.symbols('H_yy')
H_yb=sympy.symbols('H_yb')
H_b0=0#sympy.symbols('H_b0')
H_bx=sympy.symbols('H_bx')
H_by=sympy.symbols('H_by')
H_bb=sympy.symbols('H_bb')


P=sympy.Matrix([[P_00,P_0x,P_0y,P_0b],[P_x0,P_xx,P_xy,P_xb],[P_y0,P_yx,P_yy,P_yb],[P_b0,P_bx,P_by,P_bb]])

H=sympy.Matrix([[H_00,H_0x,H_0y,H_0b],[H_x0,H_xx,H_xy,H_xb],[H_y0,H_yx,H_yy,H_yb],[H_b0,H_bx,H_by,H_bb]])

def commutator(A,B):
    return (A*B)-(B*A)

print commutator(P,H)[3,1]

#print mu



