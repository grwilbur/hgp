#!/bin/bash

gamma=($(seq 0.000 0.1 325))

for g in "${gamma[@]}"
do
    change=$(sed  "103s/\(^.*phi_gamma.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $g  \2/" config.ini)

    echo -e "$change" > "config.ini"
    echo -e "$change" | grep -n "phi_gamma" | grep 103
    #python2 main.py
    #Sz=$(tail -n 1 ./data/dynamics/S.txt | cut -d " " -f 3)
    fidelity=$(python2 main.py | grep 'Fidelity' | cut -d '=' -f 2)
    echo "$fidelity"
    echo -e "$g \t $fidelity" >> "results_varyAlpha.txt"

done



