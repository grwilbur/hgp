# DYNAMICS OF DENSITY MATRIX ELEMENTS

tarray.txt: time steps in fs (1 column)
rho_i.txt: density matrix elements for dot i - as a function of time (16 columns)

# CONTENTS OF rho_i.txt
COLUMN	DENSITY MATRIX ELEMENT
0			rho_oo.real
1			rho_xx.real
2			rho_yy.real
3			rho_bb.real
4			rho_xo.real
5			rho_xo.imag
6			rho_yo.real
7			rho_yo.imag
8			rho_yx.real
9			rho_yx.imag
10			rho_bx.real
11			rho_bx.imag
12			rho_by.real
13			rho_by.imag
14			rho_bo.real
15			rho_bo.imag
