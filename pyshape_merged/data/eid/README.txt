# DOT EID CHARACTERISTICS

	Omega_eid.txt: rabi frequency (1 column) (meV)
	K_real.txt: real part of eid kernel (1 column) (ps-1)
	K_imag.txt: imag part of eid kernel (1 column) (meV)

	plot Omega_eid vs K_real or K_imag
