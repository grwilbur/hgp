[run]
	dump = True	# write parameters to screen
	t_start = 0.0	# start time for integration (fs)
	t_end = 30000.0	# start time for integration (fs)
	t_step = 1.0	# desired step time for integration (fs)
	dephasing = False	# turn on constant dephasing (parameters defined in qdot)
	phonon_eid = True	# turn on LA phonon excitation induced dephasing
	wl_eid = False	# turn on wetting layer excitation induced dephasing
	rwa = True	# use the rotating wave approximation
	mask = phasemask_poly	# choose mask: phasemask_chirp, phasemask_cos,  or ampmask_chen
	NITER = 5	# number of initial vectors to optimize
	gate = twolevel	# choose gate to implement: twolevel, onedot, twodot, threedot, crot
	optimize = False	# optimize gate fidelity and get time dependence (True) or just get time dependence (False)
	sobol_seed = 0	# seed to begin sobol sequence (used if multiple instances of code are being used to explore parameter space)
	show_plot = True	# show the dynamics plot
[rkdp]
	atol = 0.0001	# absolute tolerance for truncation error
	rtol = 0.0001	# relative tolerance for truncation error
	nsteps = 10000	# max number of steps for integration
	h_start = 0.0001	# initial step size for integration (fs)
	h_min = 0.0	# minimum step size for integration (fs)
	h_max = 1.0	# maximum step size for integration (fs)
	tdep_step = 100.0	# time step for time dependence calculation

[masks]
	[[ampmask_chen]]
		# parameter list that determines which parameters get optimized
		param_list = p1width, p2width, Aratio, pulse_area
		
		p1width = 199.289692319	# temporal width of 1st pulse (fs)
		p2width = 208.692225684	# temporal width of 2nd pulse (fs)
		Aratio = 0.717751811133	# relative amplitude of second pulse
		pulse_area = 2.0	# pulse area (radians) (as a fraction of PI)
		
		# conversion factors (TO_ARU, FROM_ARU)
		p1width_ARU = FEMTO_TO_ARU, ARU_TO_FEMTO, fs
		p2width_ARU = FEMTO_TO_ARU, ARU_TO_FEMTO, fs
		Aratio_ARU = NONE, NONE, ""
		pulse_area_ARU = AREA_TO_ARU, ARU_TO_AREA, PI radians
		
		# bounds for parameters
		p1width_range = 150.0, 300.0	# limits for pulse width (fs)
		p2width_range = 150.0, 300.0	# limits for pulse width (fs)
		Aratio_range = 0.0, 1.0	# limits for relative amplitude of second gaussian
		pulse_area_range = 0.5, 8.0	# limits for pulse area (radians) (as a fraction of PI)
	
	[[phasemask_cos]]
		# parameter list that determines which parameters get optimized
		param_list = phi_alpha, phi_gamma, phi_delta, pulse_area
		
		phi_alpha = 10.8685613057	# alpha term (radians) (as a fraction of PI)
		phi_gamma = 390.265688593	# gamma term (fs) 
		phi_delta = -0.41148380551	# relative amplitude of second pulse
		pulse_area = 0.2	# pulse area (radians) (as a fraction of PI)
		
		# conversion factors (TO_ARU, FROM_ARU)
		phi_alpha_ARU = FRAC_TO_ANGLE, ANGLE_TO_FRAC, PI radians
		phi_gamma_ARU = FEMTO_TO_ARU, ARU_TO_FEMTO, fs
		phi_delta_ARU = FRAC_TO_ANGLE, ANGLE_TO_FRAC, PI radians
		pulse_area_ARU = AREA_TO_ARU, ARU_TO_AREA, PI radians
		
		# bounds for parameters
		phi_alpha_range = 0.5, 1.0	# limits for alpha term (radians) (as a fraction of PI)
		phi_gamma_range = 200.0, 325.0	# limits for gamma term (fs) 
		phi_delta_range = -1.0, 1.0	# limits for alpha term (radians) (as a fraction of PI)
		pulse_area_range = 0.5, 2.5	# limits for pulse area (radians) (as a fraction of PI)
	
	[[phasemask_poly]]
		# parameter list that determines which parameters get optimized
		param_list = phi_2, phi_3, phi_4, pulse_area
		
		phi_2 = 10000000.0	# linear chirp (fs^2)
		phi_3 = 0.0	# quadratic chirp (fs^3)
		phi_4 = 0.0	# cubic chirp (fs^4)
		pulse_area = 5.0	# pulse area (radians) (as a fraction of PI)
		
		# conversion factors (TO_ARU, FROM_ARU)
		phi_2_ARU = FEMTO2_TO_ARU, ARU_TO_FEMTO2, fs^2
		phi_3_ARU = FEMTO3_TO_ARU, ARU_TO_FEMTO3, fs^3
		phi_4_ARU = FEMTO4_TO_ARU, ARU_TO_FEMTO4, fs^4
		pulse_area_ARU = AREA_TO_ARU, ARU_TO_AREA, PI radians
		
		# bounds for parameters
		phi_2_range = 0.0, 30000.0	# limits for linear chirp (fs^2)
		phi_3_range = 0.0, 30000.0	# limits for quadratic chirp (fs^3)
		phi_4_range = 0.0, 0.0	# limits for quadratic chirp (fs^4)
		pulse_area_range = 0.5, 2.5	# limits for pulse area (radians) (as a fraction of PI)

# parameters for laser pulse
[pulse]
	omega_o = 1.069829	# center frequency of pulse (eV)
	detun = 0.0	# pulse detuning from omega_yo (eV)
	width = 4000.0	# 117.0 229.0	# pulse width (fs)
	delay = 15000.0	# delay to center of pulse (fs)
	delta_t = 0.1	# (max) desired sampling step size (fs)
	chirp = 0.0	# gamma constant
	shape = GAUSSIAN	# pulse profile (options are: GAUSSIAN or SECH)
	pol = POL_V	# POL_H, POL_V, POL_LCP, POL_RCP, POL_DIA, POL_ADIA
	phase = 0.0	# pulse phase (as a fraction of PI)
	area = 2.0	# pulse area (radians) (as a fraction of PI) defined according to pulse_shape, pulse_width, and dipole
	dipole = 24.2
	
	unshapedfile_t = data/unshaped_pulse_t.txt	# unshaped pulse time domain output filename
	unshapedfile_f = data/unshaped_pulse_f.txt	# unshaped pulse freq domain output filename
	shapedfile_t = data/shaped_pulse_t.txt	# shaped pulse time domain output filename
	shapedfile_f = data/shaped_pulse_f.txt	# shaped pulse freq domain output filename



# quantum dot parameters
[qdot]
	omega_yo = 1.069829	# ground state to x-exciton transition energy (eV)
	omega_xyfine = 0.00015	# fine structure splitting of excitonic state (eV)
	omega_bind = 0.0025	# biexciton binding energy (eV) (Sign convention: +ve value means a bound biexciton that is lower in energy)
	
	d_xo = 0.0	# ground state to x-exciton dipole moment (Debye)
	d_yo = 24.2	# ground state to y-exicton dipole moment (Debye)
	d_xy = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bo = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bx = 0.0	# x-exciton to biexciton dipole moment (Debye)
	d_by = 0.0	# y-exciton to biexciton dipole moment (Debye)
	
	T_oo = 48000.0	# ground state decay time (ps)
	T_xx = 48000.0	# x-exciton decay time (ps)
	T_yy = 48000.0	# y-exciton decay time (ps)
	T_bb = 30000.0	# biexciton decay time (ps)
	T_xo = 300000.0	# ground state : x-exciton dephasing time (ps)
	T_yo = 50.0	# ground state : y-exciton dephasing time (ps)
	T_bo = 9.99e+15	# ground state : biexciton dephasing time (ps)
	T_xy = 9.99e+15	# x-exciton : y-exciton dephasing time (ps)
	T_bx = 300.0	# biexciton : x-exciton dephasing time (ps)
	T_by = 300.0	# biexciton : y-exciton dephasing time (ps)
	eid_c = 0.026	# wetting layer eid constant (ps)
	
	initial_state = O
	desired_state = Y

# quantum dot two
[qdot2]
	omega_yo = 1.07459	# ground state to x-exciton transition energy (eV)
	omega_xyfine = 0.00015	# fine structure splitting of excitonic state (eV)
	omega_bind = 0.0025	# biexciton binding energy (eV) (Sign convention: +ve value means a bound biexciton that is lower in energy)
	
	d_xo = 0.0	# ground state to x-exciton dipole moment (Debye)
	d_yo = 29.04	# ground state to y-exicton dipole moment (Debye)
	d_xy = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bo = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bx = 0.0	# x-exciton to biexciton dipole moment (Debye)
	d_by = 0.0	# y-exciton to biexciton dipole moment (Debye)
	
	T_oo = 480.0	# ground state decay time (ps)
	T_xx = 480.0	# x-exciton decay time (ps)
	T_yy = 480.0	# y-exciton decay time (ps)
	T_bb = 300.0	# biexciton decay time (ps)
	T_xo = 300.0	# ground state : x-exciton dephasing time (ps)
	T_yo = 40.0	# ground state : y-exciton dephasing time (ps)
	T_bo = 9.99e+15	# ground state : biexciton dephasing time (ps)
	T_xy = 9.99e+15	# x-exciton : y-exciton dephasing time (ps)
	T_bx = 300.0	# biexciton : x-exciton dephasing time (ps)
	T_by = 300.0	# biexciton : y-exciton dephasing time (ps)
	eid_c = 0.0
	
	initial_state = O
	desired_state = Y

# quantum dot three
[qdot3]
	omega_yo = 1.07259	# ground state to x-exciton transition energy (eV)
	omega_xyfine = 0.00015	# fine structure splitting of excitonic state (eV)
	omega_bind = 0.0025	# biexciton binding energy (eV) (Sign convention: +ve value means a bound biexciton that is lower in energy)
	
	d_xo = 0.0	# ground state to x-exciton dipole moment (Debye)
	d_yo = 26.04	# ground state to y-exicton dipole moment (Debye)
	d_xy = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bo = 0.0	# ground state to y-exicton dipole moment (Debye)
	d_bx = 0.0	# x-exciton to biexciton dipole moment (Debye)
	d_by = 0.0	# y-exciton to biexciton dipole moment (Debye)
	
	T_oo = 480.0	# ground state decay time (ps)
	T_xx = 480.0	# x-exciton decay time (ps)
	T_yy = 480.0	# y-exciton decay time (ps)
	T_bb = 300.0	# biexciton decay time (ps)
	T_xo = 300.0	# ground state : x-exciton dephasing time (ps)
	T_yo = 10.0	# ground state : y-exciton dephasing time (ps)
	T_bo = 9.99e+15	# ground state : biexciton dephasing time (ps)
	T_xy = 9.99e+15	# x-exciton : y-exciton dephasing time (ps)
	T_bx = 300.0	# biexciton : x-exciton dephasing time (ps)
	T_by = 300.0	# biexciton : y-exciton dephasing time (ps)
	eid_c = 0.0
	
	initial_state = O
	desired_state = Y

# phonon-induced dephasing
[laphononeid]
	omega_c_eid = 0.00144	# 0.00144 cut_off frequency for dot wavefunction (eV), Ramsay uses 0.00144, Debnath uses 0.00072
	alpha_eid = 0.0272	# material parameter (ps^2), Ramsay uses 0.0272, Debnath uses 0.022
	Omega_start_eid = 1e-07	# integration limit (eV)
	Omega_end_eid = 0.008	# integration limit (eV)
	Omega_step_eid = 2e-05	# integration step (eV)
	omega_end_eid = 5.76E-3	# integration limit (eV)
	T_eid = 15.0	# temperature (K) Debnath uses 4.0 K
	read_kernel = True	# read the kernel from file if true, calculate kernel if false
