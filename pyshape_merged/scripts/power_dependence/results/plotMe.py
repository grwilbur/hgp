import numpy as np, matplotlib.pyplot as plt

data = np.loadtxt("occupation_000000.0fs2_1163.002nm_0.002000.txt")
#data2 = np.loadtxt("occupation_-300000.0fs2_1239.842nm_0.001440.txt")
plt.plot(data[:, 0], (data[:, 3]+1)/2)
#plt.plot(data2[:, 0], (data2[:, 3]+1)/2)
plt.xlabel("Pulse area ($\\pi$)", size=15)
plt.ylabel("$\\rho_{11}$", size=15)
#plt.legend(["$\\phi_2$ = 0.3 ps$^2$", "$\\phi_2$ = -0.3 ps$^2$"])
plt.show()

