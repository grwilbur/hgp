#!/bin/bash

if [ -d "results" ]
then

    
    rm -r ./results/*.txt
    
else

    mkdir results

fi


if [ -d "configs" ]
then

    rm -r ./configs/*

else
    
    mkdir configs

fi

dipole1=($(seq 24 0.3 28))
dipole2=($(seq 24 0.3 28))

i=0
#Create a directory with many config files
for d1 in "${dipole1[@]}"
do

    for d2 in "${dipole2[@]}"
    do

	change=$(sed  "169s/\(^.*d_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $d1  \2/" config.ini | sed "215s/\(^.*d_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $d2  \2/") 
	
	echo -e "$change" > "./configs/config_${i}_${d1}_${d2}.ini"
	echo -e "$change" | sed '169q;d'
	echo -e "$change" | sed '215q;d'
	i=`echo "$i + 1" | bc`
    done
done

#use paralel to run processes at the same time

parallel -j+0 --eta python2 main.py {} ">" ./results/{/.}.txt ::: ./configs/*.ini



