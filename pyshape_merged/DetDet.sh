#!/bin/bash

if [ -d "results" ]
then

    
    rm -r ./results/*.txt
    
else

    mkdir results

fi


if [ -d "configs" ]
then

    rm -r ./configs/*

else
    
    mkdir configs

fi

detuning1=($(seq -0.003 0.0004 0.003))
detuning2=($(seq -0.003 0.0004 0.003))
wl=1 #eV
i=0
#Create a directory with many config files
for det1 in "${detuning1[@]}"
do

    for det2 in "${detuning2[@]}"
    do
	w1=$(awk "BEGIN {print $det1+$wl}")
	w2=$(awk "BEGIN {print $det2+$wl}")
	change=$(sed  "164s/\(^.*omega_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $w1  \2/" config.ini | sed "210s/\(^.*omega_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $w2  \2/") 
	
	echo -e "$change" > "./configs/config_${i}_${det1}_${det2}.ini"
	echo -e "$change" | sed '164q;d'
	echo -e "$change" | sed '210q;d'
    
	#Sz=$(tail -n 1 ./data/dynamics/S.txt | cut -d " " -f 3)
	#fidelity=$(python2 main.py | grep 'Fidelity' | cut -d '=' -f 2)
	#echo "$w \t $d \t $fidelity"
	#echo -e "$det  \t w \t $d \t  $fidelity" >> "Figure2b.txt"
	i=`echo "$i + 1" | bc`
    done
done

#use paralel to run processes at the same time

parallel -j+0 --eta python2 main.py {} ">" ./results/{/.}.txt ::: ./configs/*.ini



