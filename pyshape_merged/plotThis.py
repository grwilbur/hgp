import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt("results_varyAlpha.txt")
plt.plot(data[:, 0], data[:, 1])
plt.show()
