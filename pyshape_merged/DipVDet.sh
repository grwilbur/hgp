#!/bin/bash

if [ -d "results" ]
then

    
    rm -r ./results/*.txt
    
else

    mkdir results

fi


if [ -d "configs" ]
then

    rm -r ./configs/*

else
    
    mkdir configs

fi

dipole=($(seq 24 0.5 27))
detuning=($(seq -0.002 0.0001 0.003))
wl=1 #eV
i=0
#Create a directory with many config files
for d in "${dipole[@]}"
do

    for det in "${detuning[@]}"
    do
	w=$(awk "BEGIN {print $det+$wl}")
	change=$(sed  "210s/\(^.*omega_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $w  \2/" config.ini | sed "215s/\(^.*d_yo.*=\).*[0-9]*[\.]*[0-9]*\(.*#.*$\)/\1 $d  \2/") 
	
	echo -e "$change" > "./configs/config_${i}_${d}_${det}_${w}.ini"
	echo -e "$change" | sed '210q;d'
	echo -e "$change" | sed '215q;d'
    
	#Sz=$(tail -n 1 ./data/dynamics/S.txt | cut -d " " -f 3)
	#fidelity=$(python2 main.py | grep 'Fidelity' | cut -d '=' -f 2)
	#echo "$w \t $d \t $fidelity"
	#echo -e "$det  \t w \t $d \t  $fidelity" >> "Figure2b.txt"
	i=`echo "$i + 1" | bc`
    done
done

#use paralel to run processes at the same time

parallel -j+0 --eta python2 main.py {} ">" ./results/{/.}.txt ::: ./configs/*.ini



