import ple 
import matplotlib.pyplot as plt
import sys
import numpy as np
# plt.style.use("small_sq")
plt.style.use("wide")


def normalize(data : np.ndarray) :
	data -= np.min(data)
	data /= np.max(data)
	return data


'''
ac_hole = np.loadtxt(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210830/data/autocorrelation/1160_compensated_7pxhole.txt", skiprows=2, delimiter='\t')
ac_no_hole = np.loadtxt(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210830/data/autocorrelation/1160_compensated.txt", skiprows=2, delimiter='\t')


ac_hole[:,0] -= np.median(ac_hole[:,0])
ac_hole[:,0] *= 2*3.335640952

ac_hole[:,1] = normalize(ac_hole[:,1])



ac_no_hole[:,0] -= np.median(ac_no_hole[:,0])
ac_no_hole[:,0] *= 2*3.335640952

ac_no_hole[:,1] = normalize(ac_no_hole[:,1])


plt.plot(ac_hole[:,0], ac_hole[:,1], 'k', label="hole")
plt.plot(ac_no_hole[:,0], ac_no_hole[:,1], 'r', label="no hole")
plt.xlabel("Delay (fs)")
plt.ylabel("Intensity")
plt.xlim([-500, 500])
plt.grid()
# plt.legend()
# plt.show()
plt.savefig("achole.png")
'''

'''
s_none = np.loadtxt(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210830/data/spectra/1170_no_hole.txt", skiprows=8, delimiter='\t')
s_hole = np.loadtxt(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210830/data/spectra/1170_7pxhole.txt", skiprows=8, delimiter='\t')

s_none[:, 1] = normalize(s_none[:, 1] )
s_hole[:, 1] = normalize(s_hole[:, 1] )

s_none[:,0] = s_none[:,0]*0.049872394209854 + 1144.77237019656
s_hole[:,0] = s_hole[:,0]*0.049872394209854 + 1144.77237019656

e_none = 1239.841/s_none[:,0]
e_hole = 1239.841/s_hole[:,0]
# print(e_none)

plt.plot(e_hole, s_hole[:,1], 'k')
plt.plot(e_none, s_none[:,1], 'r')
plt.xlabel("Frequency (eV)")
plt.ylabel("Intensity")
plt.grid()
plt.savefig("specnohole.png")
# plt.show()

'''





# '''
spectrum = np.loadtxt(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210830/data/spectra/1170_7pxhole.txt", skiprows=8, delimiter='\t')
PLE_data = ple.PleData(r"/home/grant/universityworkbackup/research/experiment/quantum_control/data/20161121/data/PLE")

print(PLE_data)
np.savetxt("M21S18_DotH_PLE.txt", np.array(PLE_data.compute_ple_doublet([610, 630], convert_mic=[-4.72511, 1221.92], height_lim=0.2)).transpose())

sys.exit()

PLE_data.plot_colormap()#convert_ccd = [0.027750829, 1255.853171], convert_mic=[-4.67662, 1217.5805])

data = PLE_data.compute_ple_doublet([610, 630], convert_mic=[-4.72511, 1221.92], height_lim=0.2)
data[1] = data[1]/np.max(data[1])
plt.plot(data[0], data[1], label="PLE")
plt.plot(spectrum[:, 0]*0.049872394+1144.77237, spectrum[:, 1]/np.max(spectrum[:,1]), label="Laser Spectrum")
plt.xlabel("Wavelength (nm)")
plt.legend()
# plt.savefig("ple+spectrum.png")
plt.show()
# '''

