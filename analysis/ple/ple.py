import glob
import numpy as np
from collections import defaultdict
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.signal import peak_widths 
import matplotlib.pyplot as plt
import operator



def gaussian_plus_straight(x, A, b, x0, m, c, x1):
	return A*np.exp(-b*(x-x0)**2) + m*(x-x1) + c

def two_gaussian_plus_straight(x, A0, b0, x0, A1, b1, x1, m, c):
	return A0*np.exp(-b0*(x-x0)**2)+ A1*np.exp(-b1*(x-x1)**2)+ m*x + c








class PleData:
	def __init__(self, data_folder_path):
		#----------------------------------------------------------#
		# ALL FILES IN THE DATA FOLDER ARE ASSUMED TO BE DATA FILES
		#----------------------------------------------------------#

		# Get all of the files
		data_filepaths = glob.glob(f"{data_folder_path}/*")

		# initializing the ple_data dictionary using the defaultdict 
		# thing. This ensures that the default type of each dictionary element 
		# is a list so that we can use the append method on that key. 
		self.ple_data = defaultdict(list)
		# [print(i) for i in data_filepaths]
		for f in data_filepaths:
			# Loading the data
			# print(f)
			data = np.loadtxt(f, skiprows=8)
		
			# Getting the filename and removing the filetype
			filename = f.split("/")[-1].strip(".txt")
		
			# Extracting the micrometer posisiton
			# It is assumed to be the second to last value when the filename 
			# is split on the underscores
			micrometer_pos = float(filename.split("_")[-2])
		
			# Append the intensity data to the dictionary element who's key is the 
			# micrometer position
			self.ple_data[micrometer_pos].append(data[:, 1])

			# Gets the ccd pixels numbers from the datafile
			# This is read again and again which is inefficient but whatever, 
			# it only reads it like 100 times max.
			self.ccd_pixels = data[:,0]

		
		
		
		# Making sure that each micrometer position has the same number of averages. 
		averages = set()
		for key in self.ple_data:
			averages.add(len(self.ple_data[key]))
		
		# Raises an exception if the number of averages is inconsistent 
		if (len(averages) != 1):
			raise Exception(f"Inconsistent averages among datasets.\nNumber of each averages are: {averages}")

		
		
		# Averages the data
		for key in self.ple_data:
			self.ple_data[key] = np.average(self.ple_data[key], 0)
			# This ple_data structure is the primary way to access the data

		# Sorting the data by key. This keeps everything in the right order for later
		self.ple_data = dict(sorted(self.ple_data.items(), key=operator.itemgetter(0)))




	# Plots the average emission spectrum at a given micrometer position
	# The optional convert argument makes it possible to plot emission wavelength 
	# Rather than pixel number
	def plot_spectrum(self, micrometer_pos: float, convert=[1,0]):
		# Multiplied by conversion factors to get the desired units
		xdata = self.ccd_pixels*convert[0] + convert[1] 

		# Gets the averaged data at a given micrometer position
		ydata = self.ple_data[micrometer_pos]

		plt.plot(xdata, ydata)
		plt.show()



	# Takes a range of pixels and determines the best gaussian fit 
	# at each excitation frequency. Returns the A where f(x) = A*exp(-bx^2) + c*x + d
	# where f(x) is the fitting function
	def plot_ple(self, pixel_range : list, convert_ccd = [1,0], convert_mic=[1,0], test=False):
		
		# Selecting the pixels that are in the range specified by pixel_range
		pixels_in_range = self.ccd_pixels[pixel_range[0]:pixel_range[1]]

		# Converting to the right units (or not if you don't set them)
		pixels_in_range = pixels_in_range*convert_ccd[0] + convert_ccd[1]

		# Gets the list of micrometer positions (or nm if you use the conversion factors)
		micrometer_positions = np.array(list(self.ple_data.keys()))*convert_mic[0] + convert_mic[1]

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.ple_data:
			intensity_data.append(self.ple_data[key][pixel_range[0]:pixel_range[1]])

		intensity_data = np.array(intensity_data)



		#-------------------------------------------------------------#
		# DATA FITTING

		fitting_data = list()


		for val in intensity_data:
			try:
				# Fitting this function is very hard so we need to get close to the initial params
				# We find the peaks using scipy find_peaks, this might fail if no peaks are found,
				# hence the try-catch
				peaks, _ = find_peaks(val, height=np.max(val)/2, prominence=1)
				p1 = peak_widths(val, peaks)
				print(p1)
				
				# Now, using our findpeaks, we get initial peak position and width
				initial_params = [max(val), 0.5, pixels_in_range[peaks[0]], 0, 0, 0]
				fit,_ = curve_fit(gaussian_plus_straight, pixels_in_range, val, p0=initial_params)
				# print(props)
			# If it fails we just fill the fitting data array with zeros
			except Exception as e:
				print(f"FITTING FAILED:\n {e}")
				fit = np.array([0, 0, np.mean(pixels_in_range), 0, 0, 0])


			fitting_data.append(fit)
			if test:
				plt.plot(pixels_in_range, val)
				plt.plot(pixels_in_range, gaussian_plus_straight(pixels_in_range, *initial_params))
				# plt.hlines(y=props["peak_heights"], xmin=pixels_in_range[props["left_bases"]], xmax=pixels_in_range[props["right_bases"]])
				# plt.plot(pixels_in_range, gaussian_plus_straight(pixels_in_range, *fit))
				plt.show()


		# Plot the A param of the gaussian 
		plt.plot(micrometer_positions, np.array(fitting_data)[:, 1])
		plt.show()


	def plot_ple_singlet(self, pixel_range : list, convert_ccd = [1,0], test=False, label=""):
		
		# Selecting the pixels that are in the range specified by pixel_range
		pixels_in_range = self.ccd_pixels[pixel_range[0]:pixel_range[1]]

		# Converting to the right units (or not if you don't set them)
		pixels_in_range = pixels_in_range*convert_ccd[0] + convert_ccd[1]

		# Gets the list of micrometer positions (or nm if you use the conversion factors)
		micrometer_positions = np.array(list(self.ple_data.keys()))*convert_mic[0] + convert_mic[1]

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.ple_data:
			intensity_data.append(self.ple_data[key][pixel_range[0]:pixel_range[1]])

		intensity_data = np.array(intensity_data)



		#-------------------------------------------------------------#
		# DATA FITTING


		fitting_data = list()
		for val in intensity_data:
			# Find positions of peaks
			peaks, props = find_peaks(val, height=0.9*np.max(val), width=(None, None))

			if len(peaks) != 1:
				print(f"found {len(peaks)} at position {val}.")
				fit= [0, 1, 0, 0, 1]
				continue

			fit,_ = curve_fit(gaussian_plus_straight, pixels_in_range, val,
				p0=[props["peak_heights"][0],
					2.774*props["widths"][0], 
					peaks[0]+pixel_range[0], 
					0, 
					0]
			)

			fitting_data.append(fit[0]*1.7724/(np.sqrt(fit[1])))

			if test:
				plt.plot(pixels_in_range, val)
				plt.plot(pixels_in_range, gaussian_plus_straight(pixels_in_range, *fit))
				plt.show()


		plt.plot(micrometer_positions, fitting_data, label=label)

	
	def compute_ple_doublet(self, pixel_range : list, convert_ccd = [1,0], convert_mic =[1,0], test=False, height_lim = 0.5, label=""):
		# Selecting the pixels that are in the range specified by pixel_range
		pixels_in_range = self.ccd_pixels[pixel_range[0]:pixel_range[1]]

		# Converting to the right units (or not if you don't set them)
		pixels_in_range = pixels_in_range*convert_ccd[0] + convert_ccd[1]

		# Gets the list of micrometer positions (or nm if you use the conversion factors)
		micrometer_positions = np.array(list(self.ple_data.keys()))*convert_mic[0] + convert_mic[1]

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.ple_data:
			intensity_data.append(self.ple_data[key][pixel_range[0]:pixel_range[1]])

		intensity_data = np.array(intensity_data)



		#-------------------------------------------------------------#
		# DATA FITTING


		fitting_data = list()
		for i, val in enumerate(intensity_data):
			# Using find_peaks to get the peak positions as well as the 
			peaks, props = find_peaks(val, height=height_lim*np.max(val), width=(None, None), prominence=(None, None))

			# Defautl values of fit which give an area of zero
			fit = [0, 1, 0, 0, 1, 0, 0, 0]
			if len(peaks) < 2:
				print(f"found {len(peaks)} peaks at position {list(self.ple_data.keys())[i]}.")
				print("Setting the peak area at this point to zero.")
			
			elif len(peaks) > 2:
				print(f"found {len(peaks)} peaks at position {list(self.ple_data.keys())[i]}.")
				print("Fitting to the two largest peaks.")
				largest = props["peak_heights"].argsort()[-2:]
				new_peaks = [peaks[largest[0]], peaks[largest[1]]]
				
				new_props = dict()
				for prop in props:
					new_props[prop] = [props[prop][largest[0]], props[prop][largest[1]]]
				try:
					fit,_ = curve_fit(two_gaussian_plus_straight, pixels_in_range, val,
						p0=[new_props["peak_heights"][0],
							2.774*new_props["widths"][0], 
							peaks[0]+pixel_range[0], 
							new_props["peak_heights"][1],
							2.774*new_props["widths"][1], 
							peaks[1]+pixel_range[0],
							0, 
							0]
						)
				except:
					print("Fitting did not converge.")
					plt.plot(pixels_in_range, val)
					plt.vlines(x=new_peaks+pixels_in_range[0], ymin=0, ymax = val[new_peaks], color = "C1")
					plt.show()
					fitting_data.append(0)
					continue

			else:
				fit,_ = curve_fit(two_gaussian_plus_straight, pixels_in_range, val,
					p0=[props["peak_heights"][0],
						2.774*props["widths"][0], 
						peaks[0]+pixel_range[0], 
						props["peak_heights"][1],
						2.774*props["widths"][1], 
						peaks[1]+pixel_range[0],
						0, 
						0]
				)


			if test:
				plt.plot(pixels_in_range, val)
				plt.plot(pixels_in_range, two_gaussian_plus_straight(pixels_in_range, *fit))
				plt.vlines(x=peaks+pixels_in_range[0], ymin=0, ymax = val[peaks], color = "C1")
				print(f"x={peaks+pixels_in_range[0]}, ymin=0, ymax = {val[peaks]}")
				plt.show()
			
			fitting_data.append(fit[0]*1.7724/(np.sqrt(fit[1]))+fit[3]*1.7724/(np.sqrt(fit[4])))
		return [micrometer_positions, fitting_data]



	def plot_ple_doublet(self, pixel_range : list, convert_ccd = [1,0], convert_mic =[1,0], test=False, height_lim = 0.5, label=""):
		micrometer_positions, fitting_data = self.compute_ple_doublet(pixel_range, convert_ccd=convert_ccd, convert_mic=convert_mic, test=test, height_lim=height_lim, label="")
		plt.plot(micrometer_positions, fitting_data, label=label)
		



	def simple_plot_ple(self, pixel_number : int, convert_ccd = [1,0], convert_mic=[1,0], name="default"):
		
		# Selecting the pixels that are in the range specified by pixel_range

		# Gets the list of micrometer positions (or nm if you use the conversion factors)
		micrometer_positions = np.array(list(self.ple_data.keys()))*convert_mic[0] + convert_mic[1]

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.ple_data:
			intensity_data.append(self.ple_data[key][pixel_number])


		tmp = np.array([micrometer_positions, intensity_data]).transpose()
		# print(tmp)
		# intensity_data = np.array(intensity_data)
		# np.savetxt(name+".txt", tmp, delimiter=',', fmt='%d')
		plt.plot(micrometer_positions, intensity_data)
		plt.xlabel("excitation wavelength (nm)")
		plt.ylabel("PL intensity")
		# plt.show()




	# Plots excitation wavelength and emission wavelength 
	def plot_colormap(self, convert_ccd=[1,0], convert_mic=[1,0]):
		# Creating grids for the ccd and excitation wavelength in order to create the pcolormesh
		# X, Y = np.meshgrid(ccd_wvl, excitation_wvl)
		X, Y = np.meshgrid(np.array(self.ccd_pixels)*convert_ccd[0] + convert_ccd[1], np.array(list(self.ple_data.keys()))*convert_mic[0] + convert_mic[1])

		
		plot_data = list()
		# Turning the values of the dictionary into a 2-d array again, for pcolormeshing 
		for pos in self.ple_data:
			plot_data.append(self.ple_data[pos])
		
		plot_data = np.array(plot_data)
		
		# Plotting
		plt.pcolormesh(X, Y, plot_data, cmap="inferno", shading="nearest")
		plt.colorbar()
		plt.xlabel("emission wavelength (nm)")
		plt.ylabel("excitation wavelength (nm)")
		plt.show()



