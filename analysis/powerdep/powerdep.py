import glob
import numpy as np
from collections import defaultdict
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.signal import peak_prominences
from scipy.signal import peak_widths
import matplotlib.pyplot as plt
import operator





def gaussian_plus_straight(x, A, b, x0, m, c):
	return A*np.exp(-b*(x-x0)**2) + m*x + c

def two_gaussian_plus_straight(x, A0, b0, x0, A1, b1, x1, m, c):
	return A0*np.exp(-b0*(x-x0)**2)+ A1*np.exp(-b1*(x-x1)**2)+ m*x + c


class PowerDepData:
	def __init__(self, data_folder_path):
		#----------------------------------------------------------#
		# ALL FILES IN THE DATA FOLDER ARE ASSUMED TO BE DATA FILES
		# FILES SHOULD BE NAMED: (power)_(number).txt
		# EG: 100_2.txt => 100uW of power, second average
		#----------------------------------------------------------#

		# Get all of the files
		data_filepaths = glob.glob(f"{data_folder_path}/*")
		# print(data_filepaths)

		# initializing the powerdep_data dictionary using the defaultdict 
		# thing. This ensures that the default type of each dictionary element 
		# is a list so that we can use the append method on that key. 
		self.powerdep_data = defaultdict(list)
		
		for f in data_filepaths:
			# Loading the data
			data = np.loadtxt(f, skiprows=8)
		
			# Getting the filename and removing the filetype
			filename = f.split("/")[-1].strip(".txt")
		
			# Extracting the power
			# It is assumed to be the second to last value when the filename 
			# is split on the underscores
			power = float(filename.split("_")[-2])
		
			# Append the intensity data to the dictionary element who's key is the 
			# power
			self.powerdep_data[power].append(data[:, 1])

			# Gets the ccd pixels numbers from the datafile
			# This is read again and again which is inefficient but whatever, 
			# it only reads it like 100 times max.
			self.ccd_pixels = data[:,0]

		
		
		
		# Making sure that each power has the same number of averages. 
		averages = set()
		for key in self.powerdep_data:
			# print(key, len(self.powerdep_data[key]))
			averages.add(len(self.powerdep_data[key]))
		
		# Raises an exception if the number of averages is inconsistent 
		if (len(averages) != 1):
			print(sorted(list(map(lambda x: x.split("/")[-1].strip(".txt"), data_filepaths))))
			raise Exception(f"Inconsistent averages among datasets.\nNumber of each averages are: {averages}")


		
		
		# Averages the data
		for key in self.powerdep_data:
			self.powerdep_data[key] = np.average(self.powerdep_data[key], 0)
			# This powerdep_data structure is the primary way to access the data

		# Sorting the data by key. This keeps everything in the right order for later
		self.powerdep_data = dict(sorted(self.powerdep_data.items(), key=operator.itemgetter(0)))




	# Plots the average emission spectrum at a given power
	# The optional convert argument makes it possible to plot emission wavelength 
	# Rather than pixel number
	def plot_spectrum(self, power: float, convert=[1,0]):
		# Multiplied by conversion factors to get the desired units
		xdata = self.ccd_pixels*convert[0] + convert[1] 


		ydata = self.powerdep_data[power]

		plt.plot(xdata, ydata)
		plt.xlabel("pixel")
		plt.ylabel("counts")
		return [xdata, ydata]
		# plt.savefig("file.png")



	def plot_powerdep_singlet(self, pixel_range : list, convert_ccd = [1,0], test=False, area=True, label=""):
		
		# Selecting the pixels that are in the range specified by pixel_range
		pixels_in_range = self.ccd_pixels[pixel_range[0]:pixel_range[1]]

		# Converting to the right units (or not if you don't set the fitting params)
		pixels_in_range = pixels_in_range*convert_ccd[0] + convert_ccd[1]

		# Gets the list of powers
		powers = np.array(list(self.powerdep_data.keys()))

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.powerdep_data:
			intensity_data.append(self.powerdep_data[key][pixel_range[0]:pixel_range[1]])

		intensity_data = np.array(intensity_data)



		#-------------------------------------------------------------#
		# DATA FITTING


		fitting_data = list()
		for val in intensity_data:
			# Find positions of peaks
			peaks, props = find_peaks(val, height=0.9*np.max(val), width=(None, None))


			fit,_ = curve_fit(gaussian_plus_straight, pixels_in_range, val,
				p0=[props["peak_heights"][0],
					2.774*props["widths"][0], 
					peaks[0]+pixel_range[0], 
					0, 
					0]
			)

			fitting_data.append(fit[0]*1.7724/(np.sqrt(fit[1])))

			if test:
				plt.plot(pixels_in_range, val)
				plt.plot(pixels_in_range, gaussian_plus_straight(pixels_in_range, *fit))
				plt.show()


		if area:
			plt.plot(np.sqrt(powers), fitting_data, label=label)
		else:
			plt.plot(powers, fitting_data, label=label)

	def compute_powerdep_doublet(self, pixel_range : list, convert_ccd = [1,0], convert_mic =[1,0], test=False, height_lim = 0.5, label=""):
		# Selecting the pixels that are in the range specified by pixel_range
		pixels_in_range = self.ccd_pixels[pixel_range[0]:pixel_range[1]]

		# Converting to the right units (or not if you don't set the fitting params)
		pixels_in_range = pixels_in_range*convert_ccd[0] + convert_ccd[1]

		# Gets the list of powers
		powers = np.array(list(self.powerdep_data.keys()))

		# Gets the emission intensity data in the range we've selected
		intensity_data = list()
		for key in self.powerdep_data:
			intensity_data.append(self.powerdep_data[key][pixel_range[0]:pixel_range[1]])

		intensity_data = np.array(intensity_data)



		#-------------------------------------------------------------#
		# DATA FITTING


		fitting_data = list()
		for i, val in enumerate(intensity_data):
			# General idea here:

			# Fitting to the double gaussian is a bad, bad time if we don't have good initial guesses
			# In order to overcome this, we have to use some sneaky techniques:
			# 
			# 1. We use scipy.signal.find_peaks to find the position of all the peaks in the 
			# uPL with a certain height, specified by the height_lim argument passed to the method
			# 
			# 2. a) If fewer than two peaks are found, quit fitting for this power. This usually 
			# 		indicates that height lim is set too low or that there are not enough peaks to fit to;
			# 		this happens when power is very low and little excitation/emission happes. 
			# 	 b) If more than 2 peaks are found, fit the double gaussian to the largest two peaks. 
			#		We use the width info to get a better guess at the peak height
			# 	    If this fails, just set the area for that power to 0
			# 	 b) If exactly 2 peaks are found, fit them to the double gaussian. Same process 
			# 		for a failure
			#
			# 3. Return a 2xn array, where n is the number of pulse areas used in the experiments. 
			#	 array[0] are the pulse areas and array[1] are the associated PL intensity. 
			# 

			# removing background: makes fitting more consistent
			val -= np.min(val)

			# Using find_peaks to get the peak positions as well as the
			peaks, props = find_peaks(val, height=height_lim*np.max(val), width=(None, None), prominence=(None, None))



			# Default values of fit which give an area of zero
			fit = [0, 1, 0, 0, 1, 0, 0, 0]
			if len(peaks) < 2:
				print(f"found {len(peaks)} peaks at position {list(self.powerdep_data.keys())[i]}.")
				print("Setting the peak area at this point to zero.")
			
			# If the peakfinder finds 2 peaks
			elif len(peaks) > 2:
				print(f"found {len(peaks)} peaks at position {list(self.powerdep_data.keys())[i]}.")
				print("Fitting to the two largest peaks.")

				# sorting the peaks by height from largest to smallest
				largest = props["peak_heights"].argsort()[-2:]
				# Getting peak positions 
				new_peaks = [peaks[largest[0]], peaks[largest[1]]]
				
				new_props = dict()
				for prop in props:
					new_props[prop] = [props[prop][largest[0]], props[prop][largest[1]]]
				try:
					fit,_ = curve_fit(two_gaussian_plus_straight, pixels_in_range, val,
						p0=[new_props["peak_heights"][0],
							2.774*new_props["widths"][0], 
							peaks[0]+pixel_range[0], 
							new_props["peak_heights"][1],
							2.774*new_props["widths"][1], 
							peaks[1]+pixel_range[0],
							0, 
							0]
						)
				except:
					print(f"FITTING DID NOT CONVERGE POWER {list(self.powerdep_data.keys())[i]}.")
					# plt.plot(pixels_in_range, val)
					# plt.vlines(x=new_peaks+pixels_in_range[0], ymin=0, ymax = val[new_peaks], color = "C1")
					# plt.show()
					fitting_data.append(0)
					continue

			else:
				fit,_ = curve_fit(two_gaussian_plus_straight, pixels_in_range, val,
					p0=[props["peak_heights"][0],
						2.774*props["widths"][0], 
						peaks[0]+pixel_range[0], 
						props["peak_heights"][1],
						2.774*props["widths"][1], 
						peaks[1]+pixel_range[0],
						0, 
						0]
				)

			# The test flag will plot all of the fittings on top of the uPL so that you can see 
			# which are failing and which fits are working. This can inform your choice of 
			# height_lim
			if test:
				plt.plot(pixels_in_range, val)
				xvalues = np.linspace(pixels_in_range[0], pixels_in_range[-1], 300)
				plt.plot(xvalues, two_gaussian_plus_straight(xvalues, *fit))
				plt.vlines(x=peaks+pixels_in_range[0], ymin=0, ymax = val[peaks], color = "C1")
				print(f"x={peaks+pixels_in_range[0]}, ymin=0, ymax = {val[peaks]}")
				plt.show()
			
			fitting_data.append(fit[0]*1.7724/(np.sqrt(fit[1]))+fit[3]*1.7724/(np.sqrt(fit[4])))
		return [np.sqrt(powers), fitting_data]



	def plot_powerdep_doublet(self, pixel_range : list, convert_ccd = [1,0], convert_mic =[1,0], test=False, height_lim = 0.5, label=""):
		areas, fitting_data = self.compute_powerdep_doublet(pixel_range, convert_ccd=convert_ccd, convert_mic=convert_mic, test=test, height_lim=height_lim, label="")
		plt.plot(areas, fitting_data, label=label)
		

	# Plots powers and emission wavelength 
	def plot_colormap(self, convert_x=[1,0], area=False):
		# Creating grids for the ccd and powers in order to create the pcolormesh
		# X, Y = np.meshgrid(ccd_wvl, excitation_wvl)
		if not area:
			X, Y = np.meshgrid(np.array(self.ccd_pixels)*convert_x[0] + convert_x[1], np.array(list(self.powerdep_data.keys())))
		else:
			X, Y = np.meshgrid(np.array(self.ccd_pixels)*convert_x[0] + convert_x[1], np.sqrt(np.array(list(self.powerdep_data.keys()))))


		plot_data = list()
		# Turning the values of the dictionary into a 2-d array again, for pcolormeshing 
		for pos in self.powerdep_data:
			plot_data.append(self.powerdep_data[pos])
		
		plot_data = np.array(plot_data)
		
		# Plotting
		plt.pcolormesh(X, Y, plot_data, cmap="inferno", shading="nearest")
		plt.colorbar()
		# plt.show()


