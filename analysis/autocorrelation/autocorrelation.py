
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


# Sech^2 function used for fitting etc.
def sech2(x : float, P0 : float, width : float, x0 : float, offset : float) -> float:
	cosh = np.cosh((x - x0)/width)
	return P0/(cosh**2) + offset

# computes the fwhm of a sech^2 pulse from the width param tau where:
# P(t) = P(0) * sech^2(-t/tau)
def sech2_width(width_param : float) -> float:
	return 1.76274*width_param




# Fits a give peaked function to a set of autocorrelation data. 
def curve_fit_peak(callable, data : np.array) -> np.array:
	x_data = data[:, 0]
	y_data = data[:, 1]
	
	P0_guess     = np.max(y_data) - np.min(y_data) 
	width_guess  = 0.1 # random but seems to work most of the time
	x0_guess     = np.mean(x_data)
	offset_guess = np.min(data)

	# no need for pcov here, so long as the fit works we are happy
	popt, _ = curve_fit(callable, x_data, y_data, [P0_guess, width_guess, x0_guess, offset_guess])

	return popt



# The autocorrelation class reads a file taken using the pump-probe
# setup and processes it. It removes background, normalizes it, sets
# zero-delay properly and computes the width of the autocorrelation.
# It can also plot the data as well as a best-fit to the data using a 
# sech^2 pulse shape.  
''' Example usage:

	data = autocorrelation("/home/grant/autocorrelation_1.txt")
	# printing the pulse duration to the console
	print(data.duration()) 
	
	# plotting raw data and fit for comparison
	data.plot()
	data.plot_fit()
	plt.show()
'''
class autocorrelation:

	def __init__(self, filename, skiprows=2):
		# reading an unpacking raw data from the file
		raw_data = np.loadtxt(filename, skiprows=skiprows)

		self.delay     = raw_data[:, 0]
		self.intensity = raw_data[:, 1]
		# self.phase     = raw_data[:, 2]

		# fitting the data so that we can  do all our shifts and shit
		p0, self.ac_width, x0, offset = curve_fit_peak(sech2, raw_data)

		# removing background
		self.intensity -= offset  # offset = 0

		# nomalizing intensity to 1
		self.intensity /= p0  # p0 = 1

		# setting delay to zero
		self.delay     -= x0 # x0 = 0

		# Converting from ps to fs
		self.delay     *= 1000
		self.ac_width  *= 1000

	# Compute pulse duration from autocorrelation with sech^2 profile
	def pulse_duration(self):
		return sech2_width(self.ac_width)/1.54

	# Plot the intensity vs delay in ps
	def plot(self, label=None):
		plt.plot(self.delay, self.intensity, label=label)

	# plots the best-fit sech^2 line
	def plot_fit(self, label=None):
		plt.plot(self.delay, sech2(self.delay, 1, self.ac_width, 0, 0), label=label)


