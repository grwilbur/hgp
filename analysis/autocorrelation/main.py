import autocorrelation as ac
ac.plt.style.use("wide")


data = ac.autocorrelation("/home/grant/Downloads/2022-05-24 10:17:33.370/20220520/data/autocorrelation/3rd_spot_SEXY_ASS_autocorrelation_2.txt")

data.plot(label="Data")
data.plot_fit(label="sech$^2$ fit")
ac.plt.legend()
ac.plt.xlabel("Delay (fs)")
textstr = '\n'.join((
    r'FWHM = %.1f fs' % (data.pulse_duration()*1.54, ),
    r'Duration = %.1f fs' % (data.pulse_duration(), )))
ac.plt.hlines(0.5, -data.pulse_duration()*1.54/2, data.pulse_duration()*1.54/2)
ac.plt.text(300, 0.47, textstr, bbox=dict(boxstyle='round', facecolor='white', alpha=0.2))
ac.plt.ylabel("Intensity")
# ac.plt.show()
ac.plt.savefig("figure.png")
