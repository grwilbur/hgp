
import pyvisa as pv

import time


def ser_wait():
	time.sleep(50)

class SP2758():
	def __init__(self, address : str):
		'''
			Contructs a SP2758 object to control the pi sp2758 spectrometer

			Arguements:
				address: Serial address of the SP2758 (str)

			Return:
				None
		'''
		rm = pv.ResourceManager()

		self.spectrometer = rm.load_resource(address)





	def set_grating(self, grating: int):
		"""
			Sets the to the desired value

			Arguments:
				grating: The desired grating (int 1-9)

			Returns:
				None
		"""
		self.spectrometer.query("{} GRATING".format(grating))
		ser_wait()




	def get_grating(self) -> int:
		'''
			Get the current grating 

			Arguemnts:
				None

			Returns:
				grating (int) The current selected grating
		'''


		val = self.spectrometer.query("?GRATING")
		ser_wait()
		
		return val







	def set_center_wavelength(self, wavelength: float):
		"""
			Sets the to the desired value

			Arguments:
				wavelength: The desired wavelength (float)

			Returns:
				None
		"""


		self.spectrometer.query("{} GOTO".format(wavelength))
		ser_wait()






	def get_center_wavelength(self) -> float:
		'''
			Get the current center_wavelength 

			Arguemnts:
				None

			Returns:
				center_wavelength (float) The current selected center_wavelength
		'''
		nm_string = self.spectrometer.query("?NM").strip("nm")
		ser_wait()
		return float(nm_string)





	def set_input_slit(self, input_slit: str):
		"""
			Sets the to the desired value

			Arguments:
				input_slit: The desired input_slit (FRONT or SIDE)

			Returns:
				None
		"""
		pass





	def get_input_slit(self) -> int:
		'''
			Get the current input_slit 

			Arguemnts:
				None

			Returns:
				input_slit (int) The current selected input_slit
		'''
		pass






	def set_output_slit(self, output_slit: str):
		"""
			Sets the to the desired value

			Arguments:
				output_slit: The desired output_slit (FRONT or SIDE)

			Returns:
				None
		"""
		self.spectrometer.query("EXIT-MIRROR")
		ser_wait()

		slit_name = output_slit.upper()

		if (slit_name != "FRONT") or (slit_name != "SIDE"):
			raise EOFError("not a valid output slit")
		
		self.spectrometer.query("{}".format(slit_name))
		ser_wait()
		





	def get_output_slit(self) -> int:
		'''
			Get the current output_slit 

			Arguemnts:
				None

			Returns:
				output_slit (int) The current selected output_slit
		'''
		pass