
import pyvisa as pv


class PM100D():
	def __init__(self, address: str):
		'''
			Construts and class to interface with a PM100D powermeter

			Arguments:
				address: Serial port of the PM100D (Str)
			
			Return:
				None
		'''
		rm = pv.ResourceManager()

		self.detector = rm.open_resource(address)
	
	

	def get_power() -> float:
		'''
			Gets the power measured by the detector

			Arguemnts:
				None

			Return:
				Detector measured power in W (float)
		'''
		return self.detector.query("read?")




