

import pyvisa
import time

class ANC300(object):
    '''Module that controls each axis of the ANC300.'''
    
    def __init__(self, address, *args, **kwargs):
        # Run the (mandatory) superclass constructor
        super().__init__(*args, **kwargs)


        # Create the instrument
        rm = pyvisa.ResourceManager()
        lib = rm.visalib
        self.inst = rm.open_resource(address,
                    read_termination = '\r\n',
                    write_termination = '\r\n')


        #disable the echo of the console
        self.inst.query('echo off')

              
        #The controller is set to stepping mode for all axes
        self.inst.query('setm %d stp' %(1))
        self.inst.query('setm %d stp' %(2))
        self.inst.query('setm %d stp' %(3))
        self.inst.query('setm %d stp' %(4))

        
    def __del__(self):
        self.inst.close()

    def validate(self):
        return True

    def set_frequency(self, axis, frequency):
        '''
            Sets the frequency of the piezo of a given axis.
            
            Arguments:
                axis      (int): desired axis (1,2,3, or 4)
                frequency (int): stepping frequency (in Hz)
            
            Return:
                None
        '''
        self.inst.query('setf %d %d' %(axis,frequency))
        

    def get_frequency(self, axis):
        '''
            Gets the frequency of the piezo of a given axis.

            Arguments:
                axis (int): desired axis (1,2,3, or 4)
            
            Return:
                frequency (int): stepping frequency (in Hz)
        '''

        frequency = self.inst.query('getf %d' %(axis))
        last_line = self.inst.read()
        frequency = float(frequency[12:-2])
        return frequency

        
    def set_voltage(self, axis, voltage):
        '''
            Sets the voltage amplitude of the piezo of a given axis.
            
            Arguments:
                axis      (int): desired axis (1,2,3, or 4) 
                voltage (float): peak voltage for stepping (in Volts)
            
            Return:
                None
        '''
        self.inst.query('setv %d %f' %(axis,voltage))
        

    def get_voltage(self, axis):
        '''
        Gets the voltage amplitude of the piezo of a given axis.

        Arguments:
            axis      (int): desired axis (1,2,3, or 4) 

        Return:
            voltage (float): peak voltage for stepping (in Volts)

        '''
        voltage = self.inst.query('getv %d' %(axis))
        last_line = self.inst.read()
        voltage = float(voltage[10:-2])
        return voltage
    
    
    def step_by(self, axis, number_of_steps):
        '''
        Moves the piezo of a given axis by a given number of steps.
        
        Arguments:
            axis            (int): desired axis (1,2,3, or 4) 
            number_of_steps (int): number of steps to make
        
        Return:
            None
        '''
        if (number_of_steps > 0) :
            #Do the stepping in the upwards direction (stepu query)
            self.inst.query('stepu %d %d' %(axis,number_of_steps))
            #Increase the timeout time to an arbitrary number to be able
            #to wait for the stepwait query to finish.
            self.inst.timeout = 25000
            done = self.inst.query('stepw %d' % axis)#wait for stepping to finish
            #Reset the timeout to 30ms
            self.inst.timeout = 30
        elif number_of_steps == 0:
            pass
        else :
            #Do the stepping in the downwards direction (stepd query)
            self.inst.query('stepd %d %d' %(axis,abs(number_of_steps)))
            #Increase the timeout time to an arbitrary number to be able
            #to wait for the stepwait query to finish.
            self.inst.timeout = 25000
            done = self.inst.query('stepw %d' % axis)#wait for stepping to finish
            #Reset the timeout to 30ms
            self.inst.timeout = 30


    def ground(self):
        '''
        Grounds all the attocube piezos controlled by the ANC300

        Arguments:
            None
        
        Return:
            None
        '''
        self.inst.query('setm 1 gnd')
        self.inst.query('setm 2 gnd')
        self.inst.query('setm 3 gnd')
        self.inst.query('setm 4 gnd')

    def stop(self):
        '''
        Stops all mouvement of all the attocube piezos controlled by the ANC300

        Arguments:
            None
    
        Return:
            None
        '''
        self.inst.query('stop 1')
        self.inst.query('stop 2')
        self.inst.query('stop 3')
        self.inst.query('stop 4')

