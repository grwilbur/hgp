

'''
    **configuration**
    -- Configure monochromator
    -- Configure CCD

    **loop over masks**
        0. Set the mask using the pulse shaper

        **loop over powers**
            1. Set the power using the KDC101 & PM100D
            2. Take scan with spectrometer 
'''




import glob
import numpy as np
import time 

class pulseShaper:
    def __init__(self):
        self.ampmask = None
        self.phasemask = None
        self.compmask = None
    
    def setAmpMask(self, ampmask):
        self.ampmask = ampmask
    
    def setCompMask(self, compmask):
        self.compmask = compmask
    
    def setPhaseMask(self, phasemask):
        self.phasemask = phasemask



# Contains both the powermeter and polarization-based powercontroller 
class powercontroller:
    def __init__(self, powermeter, controller):
        self.powermeter = powermeter
        self.controller = controller

    def set_power(self, desired_power):
        measured_value = self.powermeter.get_power()
        min_diff = 10

        previous_error = 0
        integral = 0
        dt = 0.1

        while True:
            error = desired_power - measured_value

            proportional = error
            integral = integral + error * dt
            derivative = (error - previous_error) / dt

            output = Kp * proportional + Ki * integral + Kd * derivative
            previous_error = error
            time.sleep(dt)
            
        

            


class CCD:
    pass




# -------------------------
shaper = pulseShaper()

compmask = "/home/grant/programs/masks/compmask.txt"

phasemaskdir = "/home/grant/programs/masks/phasemasks/"

phasemaskfiles = glob.glob(f"{phasemaskdir}*.txt")

shaper.setCompMask(compmask)


# -------------------------
power_controller = powercontroller() 
powers = np.sqrt(np.linspace(2.25, 25, 20))



# -------------------------
cam = CCD()


for mask in phasemaskfiles:
    shaper.setPhaseMask(mask)

    for power in powers:
        power_controller.set_power(power)
        cam.acquire()
