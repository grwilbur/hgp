from simple_pid import PID
import matplotlib.pyplot as plt
import numpy as np




pid = PID(1, 0.1, 0.05, setpoint=1)

# Assume we have a system we want to control in controlled_system



# v = controlled_system.update(0)

v = 0


vs = list()
vs.append(v)


for i in range(100):
    # Compute new output from the PID according to the systems current value
    # print(v)
    control = pid(v)
    # print(control)

    # Feed the PID output to the system and get its current value
    v =  np.sqrt(0.26*control)
    vs.append(v)
    # v = controlled_system.update(control)

plt.plot(vs)
plt.show()