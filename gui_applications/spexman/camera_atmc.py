import atmcd64m
from datetime import datetime
import numpy as np

class camera:
    def __init__(self, dll_path : str, camera_index : int = 0, path_to_camera_config =""):
        self.dll = atmcd64m.atmc32d(dll_path)

        num = self.dll.get_number_of_cameras()
        assert num[0] > 0

        self.camera_handle, _ = self.dll.get_camera_handle(camera_index)

        self.dll.set_current_camera(self.camera_handle)

        error_code = self.dll.initialize(path_to_camera_config)

        assert (error_code == 20002)

        # setting acquisition mode to single-scan
        self.dll.set_acquisition_mode(1)

        self.dll.set_temperature(-60)
        self.dll.cooler_on()




    def acquire_spectrum(self, exposure_time):
        self.dll.set_read_mode(0)
        self.dll.set_exposure_time(exposure_time/1000)

        self.dll.start_acquisition()

        self.dll.wait_for_acquisition()

        data, _ = self.dll.get_acquired_data(1024)

        new_data = []
        for i in range(1024):
            new_data.append(data[i])

        return np.array(new_data)

    def acquire_image(self, exposure_time):
        self.dll.set_read_mode(4)
        self.dll.set_exposure_time(exposure_time/1000)

        self.dll.start_acquisition()

        self.dll.wait_for_acquisition()

        data, _ = self.dll.get_acquired_data(261120)


        new_data = []
        for i in range(261120):
            new_data.append(data[i])


        return np.reshape(np.array(new_data), (255, 1024))



    def get_temperature(self):
        return self.dll.get_temperature()







class spectrum:
    '''
    class for storing a full-vertically binned image. This results in a 1024x1 
    array of counts corresponding to each column of pixels on the ccd array.
    '''
    def __init__(self, wavelength, int_time, grating, counts):

        # Timestamp is always ISO8601 compliant 
        self.timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        self.wavelength       = wavelength
        self.integration_time = int_time
        self.grating          = grating
        self.counts           = counts

    def __sub__(self, o):
        return spectrum(self.counts - o.counts)

    def plot(self, axis):
        axis.plot(self.counts)
        axis.grid(True)


    def save(self, fileName, hdr):
        pixels = np.arange(0, len(self.counts), 1)
        np.savetxt(fileName, np.array([pixels, self.counts]).transpose(), header=hdr, delimiter='\t', fmt=["%d", "%d"])





class image:
    '''
    class for storing a raw image from the ccd sensor. Each datapoint in the 
    array is the number of counts measured from that pixel on the ccd. The 
    final array will have dimensions 255x1024. 
    '''
    def __init__(self, wavelength, int_time, grating, counts):

        self.timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        self.wavelength       = wavelength
        self.integration_time = int_time
        self.grating          = grating
        self.counts           = counts

    def plot(self, axis):
        axis.pcolormesh(self.counts)
        axis.grid(False)


    def save(self, fileName, hdr):
        np.savetxt(fileName, self.counts, header=hdr, delimiter='\t', fmt="%d")

