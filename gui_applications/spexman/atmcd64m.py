import ctypes

# andor_path = "C:\\Users\\lab_conputer\\programs\\andor_python_wrapper\\atmcd64d.dll"

class atmc32d:
    def __init__(self, dll_path):
        try:
            self.dll = ctypes.CDLL(dll_path)
            print("loaded", self.dll)
        except Exception as e:
            print(e)
        
    def get_number_of_cameras(self) -> tuple[int, int]:
        result = ctypes.c_long()
        code = self.dll.GetAvailableCameras(ctypes.byref(result))
        return result.value, code


    def get_camera_handle(self, camera_number: int) -> tuple[int, int]:
        handle = ctypes.c_long()

        code = self.dll.GetCameraHandle(ctypes.c_long(camera_number), ctypes.byref(handle))

        return handle.value, code

    def set_current_camera(self, handle : int) -> int:
        # print(code.value)
        error_code = self.dll.SetCurrentCamera(ctypes.c_long(handle))
        return error_code



    def initialize(self, path_to_ini : str) -> int:
        return self.dll.Initialize(path_to_ini)



    def set_read_mode(self, mode : int) -> int:
        # mode must be between 0 and 4
        assert (0 <= mode <= 4)
        return self.dll.SetReadMode(ctypes.c_long(mode))

    def set_acquisition_mode(self, mode : int) -> int:
        assert (1 <= mode <= 5)
        return self.dll.SetAcquisitionMode(ctypes.c_long(mode))
    

    def set_exposure_time(self, time : float) -> int:
        return self.dll.SetExposureTime(ctypes.c_float(time))


    def get_temperature_range(self) -> tuple[int, int, int]:
        mintemp = ctypes.c_long()
        maxtemp = ctypes.c_long()

        code = self.dll.GetTemperatureRange(ctypes.byref(mintemp), ctypes.byref(maxtemp))

        return (mintemp.value, maxtemp.value, code)
    
    def set_temperature(self, temperature) -> int:
        return self.dll.SetTemperature(ctypes.c_long(temperature))
    
    def get_temperature(self) -> tuple[int, int, int]:
        temp = ctypes.c_long()
        code = self.dll.GetTemperature(ctypes.byref(temp))

        return (temp.value, code)

    def cooler_on(self) -> int:
        return self.dll.CoolerON()


    def cooler_off(self) -> int:
        return self.dll.CoolerOFF()

    def get_detector(self) -> tuple[int, int, int] :
        xpixels = ctypes.c_long()
        ypixels = ctypes.c_long()
        code = self.dll.GetDetector(ctypes.byref(xpixels), ctypes.byref(ypixels))

        return (xpixels.value, ypixels.value, code)

    def start_acquisition(self) -> int:
        return self.dll.StartAcquisition()

    def wait_for_acquisition(self) -> int:
        return self.dll.WaitForAcquisition()
    
    def get_acquired_data(self, data_size) -> tuple[list[int], int]:
        data = (ctypes.c_long * data_size)()
        code = self.dll.GetAcquiredData(data, ctypes.c_long(data_size))

        return data, code

    def set_image(self, hbin, vibn, hstart, hend, vstart, vend) -> int:

        code = self.dll.SetImage(ctypes.c_int(hbin), 
                                    ctypes.c_int(vibn),
                                    ctypes.c_int(hstart),
                                    ctypes.c_int(hend),
                                    ctypes.c_int(vstart),
                                    ctypes.c_int(vend))
        
        return code
        


