
import numpy as np
import matplotlib.pyplot as plt
import glob


plt.style.use("wide")


def get_acdata(fname) :
	acdata = glob.glob(fname)

	data = [np.loadtxt(f, skiprows=2) for f in acdata]
	
	adata = np.mean(data, axis=0)
	
	# removing baseline
	adata[:,1] -= np.min(adata[:,1])
	# normalizing
	adata[:,1] /= np.max(adata[:,1])
	# getting zero-delay
	adata[:,0] -= adata[np.argmax(adata[:,1]),0]
	
	adata[:,0] *= 2*3.33564
	adata = adata[:, 0:2]
	return adata

# "/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210727/data/ARP/"

specdata = "/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210831/data/spectra/1163_7pxhole.txt"
# specdata = "/home/grant/universityworkbackup/research/experiment/quantum_control/data/20210831/data/spectra/1163_no-hole.txt"

data = np.loadtxt(specdata, skiprows=8)
data[:,0] *= 0.049872162671271
data[:,0] += 1134.79805350055

data[:,1] -= np.min(data[:,1])
data[:,1] /= np.max(data[:,1])

np.savetxt("spectra_1163_7pxhole.txt", data)

plt.plot(data[:,0], data[:,1])
plt.show()